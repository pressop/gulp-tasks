Pressop gulp tasks
==================

Gulp tasks used at Pressop company.

## Available tasks.
- `default`: Build all stuff.
- `watch`: Refresh tasks when a file is edited.
- `watch-auto`: Same as `watch` with an additional listener on `config.json`.
- `build-js`: Build only JS files.
- `build-css`: Build only CSS files.
- `watch-js`: Watch only JS files.
- `watch-css`: Watch only CSS files.
- `copy-files`: Copy files in the right location.
- `watch-files`: Watch files changes.

## `config.json` example

```json
{
    "output": "dist",
    "prod": true,
    "debug": false,
    "styles": {
        "bundles": [{
            "watch": [
                "styles/**/*.scss"
            ],
            "sources": [
                "styles/index.scss"
            ],
            "output": "styles.css"
        }]
    },
    "scripts": {
        "bundles": [{
            "watch": [
                "scripts/**/*.js"
            ],
            "sources": [
                "scripts/index.js"
            ],
            "output": "scripts.js"
        }]
    },
     "files": {
         "directories": [{
             "watch": [
                 "images/**/*"
             ],
             "sources": [
                 "images/**/*"
             ],
             "output": "images"
         }, {
             "watch": [
                 "fonts/**/*"
             ],
             "sources": [
                 "fonts/**/*",
                 "node_modules/**/*.eot",
                 "node_modules/**/*.ttf",
                 "node_modules/**/*.woff2",
                 "node_modules/**/*.woff"
             ],
             "output": "fonts"
         }]
     }
}
```

## Know issues
- There is a bug with sources map and babel with es2015, currently sources map is disabled for scripts.

## TODO
- Handle files processing.
- Pre-processor CSS cross import.
- Auto move file from CSS.

## License

Release under MIT license.
