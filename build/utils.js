import clc from "cli-color";

export function reportWatchChange(event) {
    let cwd = process.cwd();
    let file = event.path;

    if (file.indexOf(cwd) != -1) {
        file = file.replace(cwd, '').replace(/^[\\\/]/, '');
    }

    console.log(clc.green('--------------------------------------------------------'));
    console.log(clc.green('  File '+event.type+': ')+clc.whiteBright(file));
    console.log(clc.green('--------------------------------------------------------'));
}

export function errorHandler() {
    let args = Array.prototype.slice.call(arguments);

    args.forEach((error) => {
        console.log(clc.red('\n========================================================'));
        console.log(clc.red('  There is a problem from ')+clc.red.bold(error.plugin)+clc.red(' plugin.'));
        console.log(clc.red('========================================================\n'));

        switch (error.plugin) {
            case 'gulp-sass':
                console.log(clc.blackBright('  In file: ')+clc.whiteBright(error.relativePath));
                console.log(clc.blackBright('  On line: ')+clc.whiteBright(error.line));
                console.log(clc.blackBright('  At column: ')+clc.whiteBright(error.column));
                console.log(clc.blackBright('  Additionnal info: ')+clc.whiteBright(error.messageOriginal));
                break;
            case 'gulp-browserify':
                console.log(error.stack);
                break;
            default:
                console.log(error.toString());
        }

        console.log(clc.red('\n========================================================\n'));
    });

    this.emit('end');
}
