import path from 'path';
import fs from 'fs';
import Ajv from 'ajv';

class Config {
    /**
     * @type {Object}
     */
    configSchema;

    /**
     * @type {Function}
     */
    validate;

    /**
     * @type {string}
     */
    configFile;

    /**
     * @type {Object}
     */
    config;

    constructor(schemaFile, configFile) {
        schemaFile = path.resolve(__dirname, schemaFile);
        let ajv = new Ajv({
            useDefaults: true
        });

        this.configSchema = JSON.parse(fs.readFileSync(schemaFile, 'utf-8'));
        this.validate = ajv.compile(this.configSchema);
        this.configFile = path.resolve(__dirname, configFile);

        this.loadFile();
    }

    /**
     * Load the config from the given file.
     */
    loadFile() {
        this.config = JSON.parse(fs.readFileSync(this.configFile, 'utf-8'));

        if (!this.validate(this.config)) {
            throw new Error(JSON.stringify(this.validate.errors));
        }
    }

    /**
     * Get the config object.
     *
     * @returns {Object}
     */
    getConfig() {
        return this.config;
    }
}

export let config = new Config('configSchema.json', '../config.json');
