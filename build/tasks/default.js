import gulp from 'gulp';
import {spawn} from 'child_process';

gulp.task('default', ['build-js', 'build-css', 'copy-files']);

gulp.task('watch', ['watch-js', 'watch-css', 'watch-files']);

gulp.task('watch-auto', () => {
    let pid;

    function spawnChildren(e) {
        if(pid) {
            pid.kill();
        }

        pid = spawn(/^win/.test(process.platform) ? 'gulp.cmd' : 'gulp', ['watch'], {stdio: 'inherit'});
    }

    gulp.watch([
        'gulpfile.babel.js',
        'config.json'
    ], spawnChildren).on('error', (e) => {
        console.log(e.toString());
        this.emit('end');
    });

    spawnChildren();
});
