import gulp from 'gulp';
import combiner from 'stream-combiner2';
import {config as Config} from '../config';
import {reportWatchChange, errorHandler} from '../utils';

let config = Config.getConfig();

/**
 * Generate css tasks.
 */
let tasks = config.files.directories.map((bundle, index) => {
    let name = 'copy-file-'+index;

    gulp.task(name, () => {
        let stack = [
            gulp.src(bundle.sources),
        ];

        // Output directory.
        stack.push(gulp.dest(config.output+'/'+bundle.output));

        // Handle errors.
        let combined = combiner.obj(stack);
        combined.on('error', errorHandler);

        return combined;
    });

    return name;
});

/**
 * Generate css watch tasks.
 */
let watchTasks = config.files.directories.map((bundle, index) => {
    let name = 'watch-file-'+index;

    gulp.task(name, ['copy-file-'+index], () => {
        gulp.watch(bundle.watch, ['copy-file-'+index]).on('change', reportWatchChange);
    });

    return name;
});

gulp.task('copy-files', tasks);
gulp.task('watch-files', watchTasks);
