import gulp from 'gulp';
import del from 'del';
import vinylPaths from 'vinyl-paths';
import {config as Config} from '../config';

let config = Config.getConfig();

gulp.task('clean', () => {
    return gulp.src(config.output)
        .pipe(vinylPaths(del));
});
