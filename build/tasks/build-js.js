import gulp from 'gulp';
import combiner from 'stream-combiner2';
import browserify from 'gulp-browserify';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import {config as Config} from '../config';
import {reportWatchChange, errorHandler} from '../utils';

let config = Config.getConfig();

/**
 * Generate javascript tasks.
 */
let tasks = config.scripts.bundles.map((bundle, index) => {
    let name = 'build-js-'+index;

    gulp.task(name, () => {
        let stack = [
            gulp.src(bundle.sources),
        ];

        // KNOWN ISSUE with babel and sources map.
        // Init map.
        // if (config.debug) {
        //     stack.push(sourcemaps.init({
        //         largeFile: true
        //     }));
        // }

        // Convert ES2015 to es5 and add imported libraries.
        stack.push(browserify({
            insertGlobals : true,
            transform: ['babelify'],
            debug : config.debug
        }));

        // Concat all files.
        stack.push(concat(bundle.output));

        // Minify js.
        if (config.prod) {
            stack.push(uglify());
        }

        // KNOWN ISSUE with babel and sources map.
        // Write map.
        // if (config.debug) {
        //     stack.push(sourcemaps.write());
        // }

        // Output directory.
        stack.push(gulp.dest(config.output));

        // Handle errors.
        let combined = combiner.obj(stack);
        combined.on('error', errorHandler);

        return combined;
    });

    return name;
});

/**
 * Generate javascript watch tasks.
 */
let watchTasks = config.scripts.bundles.map((bundle, index) => {
    let name = 'watch-js-'+index;

    gulp.task(name, ['build-js-'+index], () => {
        gulp.watch(bundle.watch, ['build-js-'+index]).on('change', reportWatchChange);
    });

    return name;
});

gulp.task('build-js', tasks);
gulp.task('watch-js', watchTasks);
