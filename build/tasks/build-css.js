import gulp from 'gulp';
import combiner from 'stream-combiner2';
import sourcemaps from 'gulp-sourcemaps';
import concat from 'gulp-concat';
import cssmin from 'gulp-cssmin';
import autoprefixer from 'gulp-autoprefixer';
import gulpIf from 'gulp-if';
import sass from 'gulp-sass';
import less from 'gulp-less';
import {config as Config} from '../config';
import {reportWatchChange, errorHandler} from '../utils';

let config = Config.getConfig();

/**
 * Generate css tasks.
 */
let tasks = config.styles.bundles.map((bundle, index) => {
    let name = 'build-css-'+index;

    gulp.task(name, () => {
        let stack = [
            gulp.src(bundle.sources),
        ];

        // Init map.
        if (config.debug) {
            stack.push(sourcemaps.init({
                largeFile: true
            }));
        }

        // SASS
        stack.push(gulpIf(/\.s(c|a)ss$/, sass({
            includePaths: [
                process.cwd() + '/node_modules'
            ]
        })));

        // LESS
        stack.push(gulpIf(/\.less$/, less({
            paths: [
                process.cwd() + '/node_modules'
            ]
        })));

        // Autoprefixer, old browser compatibility.
        stack.push(autoprefixer(config.styles.autoprefixer));

        // Concat all files.
        stack.push(concat(bundle.output));

        // Minify css.
        if (config.prod) {
            stack.push(cssmin());
        }

        // Write map.
        if (config.debug) {
            stack.push(sourcemaps.write());
        }

        // Output directory.
        stack.push(gulp.dest(config.output));

        // Handle errors.
        let combined = combiner.obj(stack);
        combined.on('error', errorHandler);

        return combined;
    });

    return name;
});

/**
 * Generate css watch tasks.
 */
let watchTasks = config.styles.bundles.map((bundle, index) => {
    let name = 'watch-css-'+index;

    gulp.task(name, ['build-css-'+index], () => {
        gulp.watch(bundle.watch, ['build-css-'+index]).on('change', reportWatchChange);
    });

    return name;
});

gulp.task('build-css', tasks);
gulp.task('watch-css', watchTasks);
